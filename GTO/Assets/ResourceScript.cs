﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceScript : MonoBehaviour {

	public int StartAmount;
	public int CurrentOwned;

	// Use this for initialization
	void Start () {
		CurrentOwned = StartAmount;
	}


	public int Increase(int x){
		CurrentOwned = CurrentOwned + x;
		return CurrentOwned;
}

	public int Decrease(int x){
		CurrentOwned = CurrentOwned - x;
		return CurrentOwned;
	}
}
