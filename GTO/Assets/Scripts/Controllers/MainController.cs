﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainController : MonoBehaviour {

	public ResourceScript r;


	// Use this for initialization
	void Start () {
	}


	public int Increase
	(int x){

		return r.Increase (x);
	}

	public int Decrease(int x){
		return r.Decrease (x);
	}
}
