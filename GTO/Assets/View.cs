﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class View : MonoBehaviour {

	public Button increase;
	public Button decrease;
	public Text uittext;
    public MainController controller;

	// Use this for initialization
	void Start () {
		increase.onClick.AddListener (Increase);
		decrease.onClick.AddListener (Decrease);
	}

	public void Increase(){
		uittext.text =  controller.Increase (1).ToString();
	}

	public void Decrease (){
		uittext.text = controller.Decrease (1).ToString ();
	}
		
}